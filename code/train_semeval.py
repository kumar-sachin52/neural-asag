from lstm_emd import *
import pickle

if not os.path.exists('trained_models'):
	os.mkdir('trained_models')

if not os.path.exists('trained_models/semeval'):
	os.mkdir('trained_models/semeval')

train = pickle.load(open('data/processed/semeval/train.p','rb'))
testa = pickle.load(open('data/processed/semeval/test_unseen_answers.p','rb'))
testq = pickle.load(open('data/processed/semeval/test_unseen_questions.p','rb'))
testd = pickle.load(open('data/processed/semeval/test_unseen_domain.p','rb'))

sls = lstm("",load=False, training=True, data='semeval',num_classes=3)
sls.train_lstm(train, 200, testa)
sls.save_model('trained_models/semeval.p')

print "Unseen Answers:",sls.get_error(testa)
print "Unseen Questions:",sls.get_error(testa)
print "Unseen Domain:",sls.get_error(testa)
