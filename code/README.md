This folder contains the official code for the paper titled "Earth Mover's Distance based pooling over Siamese BiLSTMs for Automatic Short Answer Grading"

To run the code, download Google's word embeddings (word2vec) from https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/view?usp=sharing

More instructions coming soon

