import numpy as np
import numpy
import pickle
import gensim
import sys
from nltk.corpus import stopwords
d2=pickle.load(open("synsem.p",'rb'))
dtr=pickle.load(open("dwords.p",'rb'))

print "Loading Word2Vec"
from load_embeddings import *

def prepare_data(data):
    add = 0.0
    mul = 2.0
    xa1=[]
    xb1=[]
    cachedStopWords = stopwords.words('english')

    y2=[]
    y = []
    for i in range(0,len(data)):
        xa1.append(data[i][0].lower())
        xb1.append(data[i][1].lower())
        score = (float(data[i][2])+add)*mul
        y.append(float(data[i][2]))
        y2.append(int(score))

    lengths=[]
    xa12 = []
    xb12 = []
    for i in xa1:
        q = [x.lower() for x in i.split()]
        lengths.append(len(q))
        xa12.append(" ".join(q))
    for i in xb1:
        q = [x.lower() for x in i.split()]
        lengths.append(len(q))
        xb12.append(" ".join(q))
    maxlen = numpy.max(lengths)
    emb1,mas1=getmtr(xa12,maxlen)
    emb2,mas2=getmtr(xb12,maxlen)
    
    y2=np.array(y2,dtype=np.int32)
    y = np.array(y,dtype=np.float32)
    return emb1,mas1,emb2,mas2,y,y2

def prepare_data_ranking(data):
    xa1=[]
    xb1=[]
    xc1=[]
    tie=[]
    y1=[]
    y2=[]

    for i in range(0,len(data)):
        xa1.append(data[i][0])
        xb1.append(data[i][1])
        xc1.append(data[i][2])
        tie.append(int(data[i][3]))
        y1.append(int(2*float(data[i][4])))
        y2.append(int(2*float(data[i][5])))

    lengths=[]
    for i in xa1:
        lengths.append(len(i.split()))
    for i in xb1:
        lengths.append(len(i.split()))
    for i in xc1:
        lengths.append(len(i.split()))

    maxlen = numpy.max(lengths)
    emb1,mas1=getmtr(xa1,maxlen)
    emb2,mas2=getmtr(xb1,maxlen)
    emb3,mas3=getmtr(xc1,maxlen)
    
    return emb1,mas1,emb2,mas2,emb3,mas3, tie, y1, y2

def prepare_data_semeval(data):
    cachedStopWords = stopwords.words('english')
    xa1=[]
    xb1=[]
    y2=[]
    for i in range(0,len(data)):
        #y2.append(round(data[i][2],0))
        score = data[i][2]
        if score == 'correct':
	    xa1.append(data[i][0])
            xb1.append(data[i][1])
            y2.append(2)
        elif score == 'partially_correct_incomplete':
            xa1.append(data[i][0])
            xb1.append(data[i][1])
            y2.append(1)
	elif score == 'irrelevant':
	    xa1.append(data[i][0])
            xb1.append(data[i][1])
            y2.append(0)

    lengths=[]
    xa12 = []
    xb12 = []
    for i in xa1:
        q = [x.lower() for x in i.split() if True ]#x.lower() not in cachedStopWords]
        lengths.append(len(q))
        xa12.append(" ".join(q))
    for i in xb1:
        q = [x.lower() for x in i.split() if True ]#x.lower() not in cachedStopWords]
        lengths.append(len(q))
        xb12.append(" ".join(q))
    maxlen = numpy.max(lengths)
    emb1,mas1=getmtr(xa12,maxlen)
    emb2,mas2=getmtr(xb12,maxlen)
    
    y2=np.array(y2,dtype=np.float32)
    return emb1,mas1,emb2,mas2,y2,y2

def getmtr(xa,maxlen):
    n_samples = len(xa)
    ls=[]
    x_mask = numpy.zeros((maxlen, n_samples)).astype(np.float32)
    for i in range(0,len(xa)):
        q=xa[i].strip().split()
        if len(q) == 0:
            x_mask[0][i]=1
        for j in range(0,len(q)):
            x_mask[j][i]=1
        while(len(q)<maxlen):
            q.append(',')
        ls.append(q)
    xa=np.array(ls)
    return xa,x_mask


def embed(stmx):
    #stmx=stmx.split()
    dmtr=numpy.zeros((stmx.shape[0],300),dtype=np.float32)
    count=0
    if stmx[0] == ',':
        dmtr[0] = numpy.full((300,),float(sys.maxint), dtype=np.float32)
        return dmtr
    while(count<len(stmx)):
        if stmx[count]==',':
            count+=1
            continue
        if stmx[count] in dtr:
	    if dtr[stmx[count]] in model:
                dmtr[count]=model[dtr[stmx[count]]]
                count+=1
        	continue
        if stmx[count] in model:
            dmtr[count]=model[stmx[count]]
        count+=1
    return dmtr


def chsyn(s,trn):
    cachedStopWords=stopwords.words("english")
    cnt=0
    global flg
    x2=s.split()
    x=[]

    for i in x2:
        x.append(i)
    for i in range(0,len(x)):
        q=x[i]
        mst=''
        if q not in d2:
            continue
        
        if q in cachedStopWords or q.title() in cachedStopWords or q.lower() in cachedStopWords:
            #print q,"skipped"
            continue
        if q in d2 or q.lower() in d2:
            if q in d2:
                mst=findsim(q)
            #print q,mst
            elif q.lower() in d2:
                mst=findsim(q)
            if q not in model:
                mst=''
                continue

        if mst in model:
            if q==mst:
                mst=''
                
                continue
            if model.similarity(q,mst)<0.4:
                continue
            # print x[i],mst
            x[i]=mst
            if q.find('ing')!=-1:
                if x[i]+'ing' in model:
                    x[i]+='ing'
                if x[i][:-1]+'ing' in model:
                    x[i]=x[i][:-1]+'ing'
            if q.find('ed')!=-1:
                if x[i]+'ed' in model:
                    x[i]+='ed'
                if x[i][:-1]+'ed' in model:
                    x[i]=x[i][:-1]+'ed'
            cnt+=1
            # print mst
            mst=''
    return ' '.join(x),cnt

def findsim(wd):
    import random
    syns=d2[wd]
    x=random.randint(0,len(syns)-1)
    return syns[x]

def check(sa,sb,dat):
    for i in dat:
        if sa==i[0] and sb==i[1]:
            return False
        if sa==i[1] and sb==i[0]:
            return False
    return True

def expand(data):
    cachedStopWords=stopwords.words("english")
    n=[]
    for m in range(0,10):
        for i in data:
            sa,cnt1=chsyn(i[0],data)
            sb,cnt2=chsyn(i[1],data)
            if cnt1>0 and cnt2>0:
                l1=[sa,sb,i[2]]
                n.append(l1)
    print len(n)
    for i in n:
        if check(i[0],i[1],data):
            data.append(i)
    return data