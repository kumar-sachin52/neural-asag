import sys, logging, math, json
import numpy as np

class Laplace(object):
    def __init__(self, goldPath, predPath):
        with open(goldPath) as goldFile:
            gold = json.load(goldFile)
        self.qnoToGoldScores = dict()
        for qno, goldScoresStr in gold.iteritems():
            goldScoresStr = goldScoresStr.strip('[').strip(']')
            goldScores = np.array(goldScoresStr.split(','), dtype=np.float32)
            self.qnoToGoldScores[qno] = goldScores
        self.qnoToPredScores = dict()
        self.qnoToSimMat = dict()
        for qno, goldScores in self.qnoToGoldScores.iteritems():
            numAns = len(goldScores)
            self.qnoToSimMat[qno] = np.zeros([numAns, numAns], dtype=np.float32)
            self.qnoToPredScores[qno] = np.zeros(numAns, dtype=np.float32)
        with open(predPath) as predFile:
            pred = json.load(predFile)
        for qno, simRows in pred.iteritems():
            ano = 0
            for simRow in simRows:
                sims = np.array(simRow['sims'], dtype=np.float32)
                predScore = simRow['score']
                self.qnoToPredScores[qno][ano] = predScore
                for bno, simVal in enumerate(sims):
                    self.qnoToSimMat[qno][ano][bno] = simVal
                ano += 1
        # Check similarity matrices are symmetric.
        for qno, simMat in self.qnoToSimMat.iteritems():
            for ax in xrange(np.shape(simMat)[0]):
                for bx in xrange(np.shape(simMat)[1]):
                    pass#assert simMat[ax][bx] == simMat[bx][ax]        

    def oneQuestionSmooth(self, qno, alambda):
        '''
        For explanations see
        http://graphics.stanford.edu/courses/cs468-12-spring/LectureSlides/06_smoothing.pdf
        '''
        simMat = self.qnoToSimMat[qno]
        simRowSums = [np.sum(simMat[r]) for r in xrange(simMat.shape[0])]
        simLap = np.subtract(np.diag(simRowSums), simMat)
        ident = np.identity(simLap.shape[0])
        predScores = self.qnoToPredScores[qno]
        smoothScores = np.matmul(np.linalg.inv(ident + alambda * simLap),
                                 predScores)
        return smoothScores

    def allQuestionsSmooth(self, alambda):
        predGoldDiffMae, smoothGoldDiffMae = 0, 0
        numMae, numWinOrTie, numLoss = 0, 0, 0
        for qno in self.qnoToSimMat:
            smoothScores = self.oneQuestionSmooth(qno, alambda)
            goldScores = self.qnoToGoldScores[qno]
            predScores = self.qnoToPredScores[qno]
            predGoldDiffs = np.abs(predScores - goldScores)
            smoothGoldDiffs = np.abs(smoothScores - goldScores)
            assert len(predGoldDiffs) == len(smoothGoldDiffs)
            for ax, predGoldDiff in enumerate(predGoldDiffs):
                smoothGoldDiff = smoothGoldDiffs[ax]
                if math.isnan(smoothGoldDiff): continue
                numMae += 1
                predGoldDiffMae += predGoldDiff
                smoothGoldDiffMae += smoothGoldDiff
                if smoothGoldDiff <= predGoldDiff: numWinOrTie += 1
                else: numLoss += 1
        print 'allQuestionsSmooth lambda=', alambda
        print 'nq=',len(self.qnoToSimMat), 'na=',numMae, \
                   'winTie=',numWinOrTie, 'loss=',numLoss
        print 'predMae=',float(predGoldDiffMae)/numMae,\
                        'smoothMae=',float(smoothGoldDiffMae)/numMae

    def oneStepSmooth(self):
        predGoldDiffMae, nbrAvgGoldDiffMae = 0, 0
        numMae, numWinOrTie, numLoss = 0, 0, 0
        for qno, simMat in self.qnoToSimMat.iteritems():
            predScores = self.qnoToPredScores[qno]
            goldScores = self.qnoToGoldScores[qno]
            simRowSums = np.sum(simMat, axis=1)
            nbrAvgScores = np.divide(np.matmul(simMat, predScores), simRowSums)
            #nbrAvgScores = np.divide(np.matmul(predScores, simMat), simRowSums)
            nbrAvgScores = (1. * nbrAvgScores + 0. * predScores);
            predGoldDiffs = np.abs(predScores - goldScores)
            nbrAvgGoldDiffs = np.abs(nbrAvgScores - goldScores)
            #print qno, np.mean(predGoldDiffs), np.mean(nbrAvgGoldDiffs)
            assert len(predGoldDiffs) == len(nbrAvgGoldDiffs)
            for ax, predGoldDiff in enumerate(predGoldDiffs):
                nbrAvgGoldDiff = nbrAvgGoldDiffs[ax]
                if math.isnan(nbrAvgGoldDiff): continue
                numMae += 1
                predGoldDiffMae += predGoldDiff
                nbrAvgGoldDiffMae += nbrAvgGoldDiff
                if nbrAvgGoldDiff <= predGoldDiff: numWinOrTie += 1
                else: numLoss += 1
        print 'oneStepSmooth'
        print 'nq=',len(self.qnoToSimMat), 'na=',numMae, \
                   'winTie=',numWinOrTie, 'loss=',numLoss
        print 'predMae=',float(predGoldDiffMae)/numMae,\
                        'nbrAvgMae=',float(nbrAvgGoldDiffMae)/numMae

if __name__ == "__main__":
    reload(sys)
    sys.setdefaultencoding('utf-8')
    logging.basicConfig(level=logging.INFO)
    lap = Laplace('groundtruth.json', 'scores.json')
    lap.oneStepSmooth()
    for alambda in np.linspace(.001, 0.999, num=10):
        lap.allQuestionsSmooth(alambda)
