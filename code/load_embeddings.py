#loads pretrained word embeddings, currently using google word2vec
from gensim.models import KeyedVectors
#model = KeyedVectors.load_word2vec_format("data/paragram_300_sl999/paraw2v.txt", encoding='latin1', binary=False)
model = KeyedVectors.load_word2vec_format("GoogleNews-vectors-negative300.bin",binary=True)
#model = KeyedVectors.load_word2vec_format("w2v.txt",binary=False)
