import pandas as pd
import numpy

def load_data(sheet, marksheet, shuffle=True):
	scoremat = []

	trainingpairs = []
	q_idx = numpy.arange(1, len(sheet.columns))
	if shuffle:
		numpy.random.shuffle(q_idx)

	for colno in q_idx:
		training = []
		answers = sheet.icol(colno)
		marks = marksheet.icol(colno)

		model = answers[0]
		for answer, mark in zip(answers[1:], marks):
			if answer == answer and mark == mark:
				training.append((model, answer, mark))
		trainingpairs.append(training)
	return trainingpairs

def load_train_data(shuffle=True, augment_model_answer=True):
	sheet = pd.read_csv('../data/semeval/sciEntsBank_train.csv',header=0)
	marksheet = pd.read_csv('../data/semeval/sciEntsBank_train-GT.csv',header=0)
	trp = load_data(sheet, marksheet, shuffle)

	if(augment_model_answer):
		trp = expand(trp)

	trainingpairs = []
	for p in trp:
		trainingpairs += p

	return trainingpairs

def load_test_data(type='question'):
	"""
	Load the test dataset depending on the type specified. Type could be question | answer | domain. Refer to the paper for details
	"""
	if type == 'question':
		sheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-questions.csv',header=0)
		marksheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-questions-GT.csv',header=0)
	elif type == 'answer':
		sheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-answers.csv',header=0)
		marksheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-answers-GT.csv',header=0)
	elif type == 'domain':
		sheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-domains.csv',header=0)
		marksheet = pd.read_csv('../data/semeval/sciEntsBank_test-unseen-domains-GT.csv',header=0)
	else:
		print 'wrong type'
		return

	tsp = load_data(sheet, marksheet, False)
	test_pairs = []
	for p in tsp:
		test_pairs += p

	return test_pairs

def expand(trainingpairs):
	newtrainpairs = []
	for question_data in trainingpairs:
		models = []
		models.append((question_data[0][0], -1))
		for i in range(len(question_data)):
			if(question_data[i][2] == 'correct'):
				models.append((question_data[i][1], i))
		for m in models:
			newpairs = []
			for i in range(len(question_data)):
				if(i != m[1]):
					newpairs.append((m[0], question_data[i][1], question_data[i][2]))
			newtrainpairs.append(newpairs)

	return newtrainpairs

def cleanup(trainingpairs, testpairs):
	import nltk
	import string
	punct = string.punctuation
	tokenizer = nltk.tokenize.WordPunctTokenizer()
	train = []
	test = []
	c = 0

	for tr in trainingpairs:
		m = tr[0]
		a = tr[1]
		mt = tokenizer.tokenize(m)
		at = tokenizer.tokenize(a)
		m = ""
		a = ""
		for s in mt:
			s2 = str(s).translate(None, punct)
			m += s2 + " "
		for s in at:
			s2 = str(s).translate(None, punct)
			a += s2 + " "

		train.append((str(m), str(a), tr[2]))
		c+=1

	for tr in testpairs:
		m = tr[0]
		a = tr[1]
		sc = tr[2]
		mt = tokenizer.tokenize(m)
		at = tokenizer.tokenize(a)
		m = ""
		a = ""
		for s in mt:
			s2 = str(s).translate(None, punct)
			m += s2 + " "
		for s in at:
			s2 = str(s).translate(None, punct)
			a += s2 + " "
		test.append((str(m), str(a), sc))
		c+=1

   	ntrain = []
   	for tr in train:
   		ntrain.append((tr[0].replace(" br ",""), tr[1].replace(" br ",""), tr[2]))
   	train = ntrain

   	ntest = []
   	for tr in test:
   		ntest.append((tr[0].replace(" br ",""), tr[1].replace(" br ",""), tr[2]))
   	test = ntest

   	ntrain = []
   	for tr in train:
   		a1 = tr[1]
   		if ('XXXXX' in tr[1]) or ('NO ANSWER' in tr[1]) or ('not answered' in tr[1]):
   			a1 = ""
   		ntrain.append((tr[0], a1, tr[2]))
   	train = ntrain
   	ntest = []
   	for tr in test:
   		if ('XXXXX' in tr[1]) or ('NO ANSWER' in tr[1]) or ('not answered' in tr[1]):
   			ntest.append((tr[0], "", tr[2]))
   		else:
   			ntest.append(tr)
   	test = ntest

   	ntrain = []
   	test = []
   	for tr in train:
   		ntrain.append((str(tr[0]), str(tr[1]), tr[2]))
   	for tr in test:
   		ntest.append((str(tr[0]), str(tr[1]), tr[2]))

   	print len(ntrain), len(ntest)
   	return ntrain, ntest