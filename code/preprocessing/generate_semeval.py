from utils_semeval import *
import os
import pickle

def run(where="../data/processed/semeval"):
	if not os.path.exists(where):
		print "The specified path doesn't exist. Exiting ..."
		return

	train = load_train_data(False, False)
	testa = load_test_data(type='answer')
	testq = load_test_data(type='question')
	testd = load_test_data(type='domain')

	train, testa = cleanup(train, testa)
	train, testq = cleanup(train, testq)
	train, testd = cleanup(train, testd)

	pickle.dump(train, open(where+"/train.p", "wb"))
	pickle.dump(testa, open(where+"/test_unseen_answers.p", "wb"))
	pickle.dump(testq, open(where+"/test_unseen_questions.p", "wb"))
	pickle.dump(testd, open(where+"/test_unseen_domain.p", "wb"))

if __name__ == '__main__':
	run()

