import pandas as pd
import numpy

def load_data_mohler_mohler(shuffle=True):
	"""
	Reads the Mohler Dataset files. Returns them as a list of lists (question wise) where each sublist is (model_answer, student_answer, score)

	parameters: 
	shuffle (boolean): 
	"""
	sheet = pd.read_excel('data/2mohler-#QuestionsRemoved.xlsx',0)
	marksheet = pd.read_excel('data/2mohler-0-#QuestionsRemoved-GT.xlsx')
	scoremat = []

	import nltk
	import string
	punct = string.punctuation
	tokenizer = nltk.tokenize.WordPunctTokenizer()
	trainingpairs = []
	q_idx = numpy.arange(1, len(sheet.columns))
	if shuffle:
		numpy.random.shuffle(q_idx)

	for colno in q_idx:
		training = []
		answers = sheet.icol(colno)
		marks = marksheet.icol(colno)

		model_ = answers[0]
		tokens = tokenizer.tokenize(model_)
		model = ""
		for s in tokens:
			s2 = str(s).translate(None, punct)
			model += s2 + " "

		for answer_, mark in zip(answers[1:], marks):
			tokens = tokenizer.tokenize(answer_)
			answer = ""
			for s in tokens:
				s2 = str(s).translate(None, punct)
				answer += s2 + " "

			training.append((model, answer, float(mark)))
		trainingpairs.append(training)

	return trainingpairs

def get_paired_train_test(
							test_size = 0.1, 
							augment_model_answer=True
						):
	"""
	Returns two objects: training set and test set (pairwise, not ranking) for Mohler dataset. Both of them are list of (model, student, score).
	The size of test set is determined by test_size times the size of total dataset

	Parameters:
	test_size: fraction of dataset to use as test
	augment_model_answer: Augments the training set with the scheme explained in the paper.
	"""
	pairs = load_data_mohler()
	train_count = int((1-test_size)*len(pairs))
	if train_count <= 0:
		train_count = 1

	trp, tep = pairs[:train_count], pairs[train_count:]

	if(augment_model_answer):
		trp = expand(trp)

	train_pairs = []
	for train_pair in trp:
		train_pairs += train_pair

	test_pairs = []
	for test_pair in tep:
		test_pairs += test_pair
	return train_pairs, test_pairs

def get_ranking_data(
						trainingpairs, 
						test_pairs
					):
	"""
	Takes as input test and training pairs in question wise form and returns training triplets and test pairs dataset.

	training triplets are for the form (model, student1, student2, isequal, score1, score2)
	where 
	if isequal is 0, score1 > score2
	if isequal is 1, score1 = score2
	"""
	trainingtriplets = []

	print len(trainingpairs), len(test_pairs)

	for question_data in trainingpairs:
		for i in range(0,len(question_data)):
			for j in range(i+1, len(question_data)):
				if(question_data[i][2] < question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 0, question_data[j][2], question_data[i][2]))
				elif(question_data[i][2] == question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 1, question_data[j][2], question_data[i][2]))
				else:
					trainingtriplets.append((question_data[i][0], question_data[i][1], question_data[j][1], 0, question_data[i][2], question_data[j][2]))

	ntest_pairs = []
	for test_pair in test_pairs:
		ntest_pairs += test_pair
	return trainingtriplets, ntest_pairs

def get_paired_train_test_skip(
								skip, 
								augment_model_answer=True
							):
	"""
	Creates leave one question out dataset (pairwise), by leaving out question number specified by `skip`
	"""
	trainingpairs = load_data_mohler(shuffle=False)
	trainingtriplets = []
	test_pairs = trainingpairs[skip]

	trp = trainingpairs[:skip]+trainingpairs[skip+1:]
	print len(trp)

	if augment_model_answer:
		trp = expand(trp)

	train_pairs = []
	for train_pair in trp:
		train_pairs += train_pair

	return train_pairs, test_pairs

def expand(trainingpairs):
	"""
	returns paired training set after augmentation (question wise)
	"""
	newtrainpairs = []
	for question_data in trainingpairs:
		models = []
		models.append((question_data[0][0], -1))
		for i in range(len(question_data)):
			if(question_data[i][2] == 5.0):
				models.append((question_data[i][1], i))

		for m in models:
			newpairs = []
			for i in range(len(question_data)):
				if(i != m[1]):
					newpairs.append((m[0], question_data[i][1], question_data[i][2]))
		
			newtrainpairs.append(newpairs)

	return newtrainpairs

def get_train_test(
					test_size = 0.1, 
					augment_model_answer=True
				):
	"""
	Returns ranking train and test data, similar otherwise to get_paired_train_test
	"""
	pairs = load_data_mohler()
	train_count = int((1-test_size)*len(pairs))
	if train_count <= 0:
		train_count = 1

	trainingpairs, test_pairs = pairs[:train_count], pairs[train_count:]
	print len(trainingpairs)
	if augment_model_answer:
		trainingpairs = expand(trainingpairs)

	trainingtriplets = []

	print len(trainingpairs), len(test_pairs)

	for question_data in trainingpairs:
		for i in range(0,len(question_data)):
			for j in range(i+1, len(question_data)):
				if(question_data[i][2] < question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 0, question_data[j][2], question_data[i][2]))
				elif(question_data[i][2] == question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 1, question_data[j][2], question_data[i][2]))
				else:
					trainingtriplets.append((question_data[i][0], question_data[i][1], question_data[j][1], 0, question_data[i][2], question_data[j][2]))

	ntest_pairs = []
	for test_pair in test_pairs:
		ntest_pairs += test_pair
	return trainingtriplets, ntest_pairs

def get_train_test_skip(
						skip, 
						augment_model_answer=True
					):
	"""
	Returns ranking leave one question out data, similar otherwise to get_paired_train_test
	"""
	pairs = load_data_mohler(shuffle=False)
	test_pairs = pairs[skip]
	trainingpairs = pairs[:skip] + pairs[skip+1:]
	trainingtriplets = []

	if augment_model_answer:
		trainingpairs = expand(trainingpairs)

	print len(trainingpairs), len(test_pairs)

	for question_data in trainingpairs:
		for i in range(0,len(question_data)):
			for j in range(i+1, len(question_data)):
				if(question_data[i][2] < question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 0, question_data[j][2], question_data[i][2]))
				elif(question_data[i][2] == question_data[j][2]):
					trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 1, question_data[j][2], question_data[i][2]))
				else:
					trainingtriplets.append((question_data[i][0], question_data[i][1], question_data[j][1], 0, question_data[i][2], question_data[j][2]))

	return trainingtriplets, test_pairs


def sample_data(data, size = 0.1):
	idx = numpy.arange(0, len(data))
	numpy.random.shuffle(idx)
	return numpy.array(data)[idx[:int(size*len(data))]]

def cleanup(trainingpairs, testpairs):
	import nltk
	import string
	punct = string.punctuation
	tokenizer = nltk.tokenize.WordPunctTokenizer()
	train = []
	test = []
	c = 0

	for tr in trainingpairs:
		m = tr[0]
		a = tr[1]
		mt = tokenizer.tokenize(m)
		at = tokenizer.tokenize(a)
		m = ""
		a = ""
		for s in mt:
			s2 = str(s).translate(None, punct)
			m += s2 + " "
		for s in at:
			s2 = str(s).translate(None, punct)
			a += s2 + " "

		train.append((str(m), str(a), tr[2]))
		c+=1

	for tr in testpairs:
		m = tr[0]
		a = tr[1]
		sc = tr[2]
		mt = tokenizer.tokenize(m)
		at = tokenizer.tokenize(a)
		m = ""
		a = ""
		for s in mt:
			s2 = str(s).translate(None, punct)
			m += s2 + " "
		for s in at:
			s2 = str(s).translate(None, punct)
			a += s2 + " "
		test.append((str(m), str(a), sc))
		c+=1

   	ntrain = []
   	for tr in train:
   		ntrain.append((tr[0].replace(" br ",""), tr[1].replace(" br ",""), tr[2]))
   	train = ntrain

   	ntest = []
   	for tr in test:
   		ntest.append((tr[0].replace(" br ",""), tr[1].replace(" br ",""), tr[2]))
   	test = ntest

   	ntrain = []
   	for tr in train:
   		a1 = tr[1]
   		if ('XXXXX' in tr[1]) or ('NO ANSWER' in tr[1]) or ('not answered' in tr[1]):
   			a1 = ""
   		ntrain.append((tr[0], a1, tr[2]))
   	train = ntrain
   	ntest = []
   	for tr in test:
   		if ('XXXXX' in tr[1]) or ('NO ANSWER' in tr[1]) or ('not answered' in tr[1]):
   			ntest.append((tr[0], "", tr[2]))
   		else:
   			ntest.append(tr)
   	test = ntest

   	ntrain = []
   	test = []
   	for tr in train:
   		ntrain.append((str(tr[0]), str(tr[1]), tr[2]))
   	for tr in test:
   		ntest.append((str(tr[0]), str(tr[1]), tr[2]))

   	print len(ntrain), len(ntest)
   	return ntrain, ntest
