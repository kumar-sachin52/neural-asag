"""
This file creates a cleaned up version of 12 folds of Mohler data set as used in the experiments in the paper. 

All of them as saved a pickle dumps in the path specified.
"""

import pickle
from create_dataset_mohler import *

def run(expan=True,where='../data/folds/'):
	import nltk
	import string
	import os

	if not os.path.exists(where):
		os.mkdir(where)
		
	punct = string.punctuation
	tokenizer = nltk.tokenize.WordPunctTokenizer()

	pairs = pickle.load(open('../data/12fold.data','rb'))

	for k in range(12):
		trainingfolds = pairs[:k]+pairs[k+1:]
		testfold = pairs[k]

		trainingpairs_ = []
		for tr in trainingfolds:
			trainingpairs_ += tr
	
		if expan:
			trainingpairs_ = expand(trainingpairs_)



		"""
		trainingtriplets = []

		for question_data in trainingpairs_:
			for i in range(0,len(question_data)):
				for j in range(i+1, len(question_data)):
					if(question_data[i][2] < question_data[j][2]):
						trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 0, question_data[j][2], question_data[i][2]))
					elif(question_data[i][2] == question_data[j][2]):
						trainingtriplets.append((question_data[i][0], question_data[j][1], question_data[i][1], 1, question_data[j][2], question_data[i][2]))
					else:
						trainingtriplets.append((question_data[i][0], question_data[i][1], question_data[j][1], 0, question_data[i][2], question_data[j][2]))
		"""

		trainingpairs = []
		for tr in trainingpairs_:
			trainingpairs += tr

		testpairs = []
		for ts in testfold:
			testpairs += ts

		ntrain, ntest = cleanup(trainingpairs, testpairs)
		pickle.dump((ntrain, ntest), open(where+str(k)+'.fold', 'wb'))
		print where+str(k)+'.folds'

if __name__ == '__main__':
	run()