from lstm_emd import *

all_results = []
results_file = open('results.txt','w')

if not os.path.exists("./trained_models"):
	os.mkdir("./trained_models")
	
if not os.path.exists("./trained_models/mohler"):
	os.mkdir("./trainied_models/mohler")


for i in range(12):
	sls=lstm("",load=False,training=True)
	train, test = pickle.load(open('data/folds/'+str(i)+".fold", 'rb'))

	sls.train_lstm(train, test)
	results = sls.get_error(test)
	print results

	all_results.append(results)
	results_file.write(str(results)+"\n")
	sls.save_model('./trained_models/mohler/'+str(i)+'.p')

import numpy
print numpy.mean(all_results, axis=0)
results_file.close()