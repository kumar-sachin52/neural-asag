"""
Contains class for creating, training and testing the proposed model
"""
import gensim
from gensim.models import word2vec
import pickle
import numpy as np
import numpy
import pickle
from random import *
import theano.tensor as T
def _p(pp, name):
    return '%s_%s' % (pp, name)
    import re
from nltk.corpus import stopwords
import scipy.stats as meas
from gensim.models import word2vec
from collections import OrderedDict
import pickle as pkl
import random
import sys
import time
import numpy
import theano
from theano import config
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
from sentences import *
from scipy.optimize import linprog
class lstm():    
    def save_model(self, name):
        with open(name,'wb') as f:
           pickle.dump(unzip(self.tnewp), f)
    def __reverse(self, emb, mask):
        """
        Reverse the input tensor `emb` for creating BiLSTM
        Parameters:
        emb: The input tensor
        mask: the mask vector of the corresponding tensor (used for padding)
        """
        source = emb.dimshuffle(1, 0, 2)
        sz = mask.sum(axis=0).astype('int64')
        def _step(psource, psz):
            psz = tensor.switch(psz>0, psz, 1)
            return tensor.concatenate([psource[psz-1::-1, :], tensor.zeros((mask.shape[0]-psz, 300))] )
        outputs, updates = theano.scan(
            fn=_step,
            sequences=[source, sz]
            )
        return tensor.cast(outputs.dimshuffle(1, 0, 2), theano.config.floatX)
    def __init__(
                    self, 
                    nam, 
                    load=False,
                    load_emd=False, 
                    training=False, 
                    num_classes=11
                ):
        """
        Parameters:
        nam: name of the model file
        load: load the model if True
        load_emd: if True, load all the parameters except the SVOR layer. if `load` is False, this is not executed
        training: True, if want to train the model else gradients are not computed
        num_classes: number of classes in the ordinal layer, default is 11 for Mohler
        """

        newp=creatrnnx()
        for i in newp.keys():
            if i[0]=='1':
                newp['2'+i[1:]]=newp[i]

        y = tensor.ivector('y')
        #tensors and masks for model and student answers
        mask11 = tensor.matrix('mask11', dtype=config.floatX)
        mask21 = tensor.matrix('mask21', dtype=config.floatX)
        emb11=theano.tensor.ftensor3('emb11')
        emb21=theano.tensor.ftensor3('emb21')
        #reverse tensors
        rev_emb11 = self.__reverse(emb11, mask11)
        rev_emb21 = self.__reverse(emb21, mask21)
        #parameters for SVOR
        newp['FU'] = numpy.random.normal(-0.5,0.5,(1,1)).astype(config.floatX)
        newp['Fb'] = numpy.zeros((num_classes-1,)).astype(config.floatX) 

        #flows matrix for EMD
        flows = theano.tensor.ftensor3('flows')

        if load:
            if not load_emd:
                newp_partial=pickle.load(open(nam,'rb'))
                newp['1lstm1_U']=newp_partial['1lstm1_U']
                newp['1lstm1_W']=newp_partial['1lstm1_W']
                newp['1lstm1_b']=newp_partial['1lstm1_b']
                newp['1lstm2_U']=newp_partial['1lstm1_U']
                newp['1lstm2_W']=newp_partial['1lstm1_W']
                newp['1lstm2_b']=newp_partial['1lstm1_b']
                newp['2lstm1_U']=newp_partial['2lstm1_U']
                newp['2lstm1_W']=newp_partial['2lstm1_W']
                newp['2lstm1_b']=newp_partial['2lstm1_b']
                newp['2lstm2_U']=newp_partial['2lstm1_U']
                newp['2lstm2_W']=newp_partial['2lstm1_W']
                newp['2lstm2_b']=newp_partial['2lstm1_b']
            else:
                newp = pickle.load(open(nam, 'rb'))
        self.tnewp=init_tparams(newp)
        trng = RandomStreams(1234)
        use_noise = theano.shared(numpy_floatX(0.))

        rate=0.5
        rrng=trng.binomial(emb11.shape,p=1-rate, n=1,dtype=emb11.dtype)

        #create LSTMs
        proj11_all=getpl2(emb11,'1lstm1',mask11,False,rrng,50,self.tnewp)
        proj12_all=getpl2(rev_emb11,'1lstm2',mask11,False,rrng,50,self.tnewp)
        proj21_all=getpl2(emb21,'2lstm1',mask21,False,rrng,50,self.tnewp)
        proj22_all=getpl2(rev_emb21,'2lstm2',mask21,False,rrng,50,self.tnewp)
        proj1_all = T.concatenate([proj11_all, proj12_all], axis=2)
        proj2_all = T.concatenate([proj21_all, proj22_all], axis=2)
        #proj1_normalized = proj1_all/proj1_all.norm(axis=2, L=2).dimshuffle(0,1,'x')
        #proj2_normalized = proj2_all/proj2_all.norm(axis=2, L=2).dimshuffle(0,1,'x')
        #distance matrix calculation based on L1 distance
        distance_matrix = ( T.sum( T.abs_( proj1_all.dimshuffle(0,'x',1,2)-proj2_all.dimshuffle('x',0,1,2)), axis=3) )
        #distance matrix calcutation based on squared L2 distance
        #distance_matrix = ( T.sum( T.abs_( proj1_all.dimshuffle(0,'x',1,2)-proj2_all.dimshuffle('x',0,1,2)), axis=3) )
        emd = T.sum(flows*distance_matrix, axis=[0,1])
        emd = emd.reshape((-1,1))

        ################Logit OR
        # matlist = []
        # for i in range(num_classes-1):
        #     matlist.append(self.tnewp['FU'])
        # conmat = T.concatenate(matlist)
        # totalsum = T.dot(emd.reshape((-1,1)), conmat.T)+self.tnewp['Fb']
        # pred = T.nnet.sigmoid(totalsum)
        #cost = T.mean( ((pred-y)**2 ).sum(axis=1) )
        ################
        ################SVOR based Hinge Loss
        features = emd
        pred = T.sum( T.switch(T.dot(features, self.tnewp['FU']).flatten().dimshuffle(0,'x')-self.tnewp['Fb'].dimshuffle('x',0) >= 0, 1, 0) , axis=1)/2.0
        pred_dist = T.dot(features, self.tnewp['FU']).flatten().dimshuffle(0,'x')-self.tnewp['Fb'].dimshuffle('x',0)
        cost = sum([T.sum(T.switch( y <= j , T.maximum(0, 1 + (pred_dist.T)[j]), T.maximum(0, 1 - (pred_dist.T)[j]))) for j in range(num_classes-1)])
        cost += 0.5*T.sum(T.dot(self.tnewp['FU'], self.tnewp['FU'].T))
        ################
        ###############EMD to score and L2 Loss
        #sim = T.exp(-emd_l1)
        #ys=T.clip((y-0.0)/5.0,1e-7,1.0-1e-7)
        #loss = (sim-ys)**2
        #cost = T.mean(loss)
        #pred = sim*5.0
        ###############
        lr = tensor.scalar(name='lr')
        #L-x regularization, didn't help in final results
        l2_weight = 0.000
        l1_weight = 0.000
        l1_penalty = sum([T.sum(abs(temp)) for temp in self.tnewp.values()])
        l2_penalty = sum([T.sum(temp**2) for temp in self.tnewp.values()])
        cost += l2_weight*l2_penalty + l1_weight*l1_penalty

        self.f_dm = theano.function([emb11,mask11,emb21,mask21], distance_matrix, allow_input_downcast=True) 
        self.f_emd = theano.function([emb11,mask11,emb21,mask21,flows], emd, allow_input_downcast=True)    
        self.f_pred_dist = theano.function([emb11,mask11,emb21,mask21,flows],pred_dist,allow_input_downcast=True)
        self.f_pred = theano.function([emb11, mask11,emb21,mask21,flows],pred,allow_input_downcast=True)
        self.f_cost = theano.function([emb11,mask11,emb21,mask21,y,flows],cost,allow_input_downcast=True)

        if training==True:
            #compute gradients and assign the average of the parameter values to both LSTMs
            gradi = tensor.grad(cost, wrt=self.tnewp.values())#/bts
            grads=[]
            l=len(gradi)-2
            for i in range(0,l/2):
                gravg=(gradi[i]+gradi[i+l/2])/(2.0)
                grads.append(gravg)
            for i in range(0,len(self.tnewp.keys())/2-1):
                    grads.append(grads[i])
            grads.append(gradi[l])
            grads.append(gradi[l+1])

            self.f_grad_shared, self.f_update = adadelta(lr, self.tnewp, grads,emb11,mask11,emb21,mask21,y,cost,flows)

    def solve_emd(self, dm, b, d):
        """
        slow EMD calculation using scipy's linprog
        Parameters:
        ------------------
        dm: distance matrix
        b: model mask
        d: answer mask
        """
        dm = dm.swapaxes(1,2)
        dm = dm.swapaxes(0,1)
        l = dm.shape[1]
        flows = []
        for ii in range(dm.shape[0]):
            bs = b.sum(axis=0).astype(int)
            ds = d.sum(axis=0).astype(int)
            c = dm[ii][:bs[ii],:ds[ii]].flatten()
            A_eq = []
            for j in range(ds[ii]):
                row = [0 for k in range(bs[ii]*ds[ii])]
                for k in range(j*bs[ii], (j+1)*bs[ii]):
                    row[k] = 1
                A_eq.append(row)
            for j in range(bs[ii]):
                row = [0 for k in range(bs[ii]*ds[ii])]
                for k in range(0, bs[ii]*ds[ii], ds[ii]):
                    row[k] = 1
                A_eq.append(row)
            b_eq = [1.0/bs[ii] for j in range(bs[ii])]+[1.0/ds[ii] for j in range(ds[ii])]
            A_eq = np.array(A_eq)
            b_eq = np.array(b_eq)
            r = linprog(c, A_eq = A_eq, b_eq=b_eq, options=dict(maxiter=10000))
            df = r.x.reshape((bs[ii],ds[ii]))
            f = np.zeros((l,l))
            f[:bs[ii], :ds[ii]] = df
            flows.append(f)
        flows = np.array(flows, dtype=np.float32)
        flows = flows.swapaxes(0,1)
        flows = flows.swapaxes(1,2)
        return flows

    def solve_emd_fast(self, dm, b, d, lambd=0.1):
        """
        fast and approximate EMD calculation using Sinkhorn, the lambda used here is the reciprocal of lambda described in the sinkhorn paper
        """
        import ot
        dm = dm.swapaxes(1,2)
        dm = dm.swapaxes(0,1)
        bs = b.sum(axis=0).astype(int)
        ds = d.sum(axis=0).astype(int)
        l = dm.shape[1]
        #print l
        flows = []
        for ii in range(dm.shape[0]):
            #print bs[ii], ds[ii]
            if ds[ii] == 0:
                ds[ii] = 1
            c = dm[ii][:bs[ii], :ds[ii]]

            s1 = np.array([1.0/bs[ii] for kk in range(bs[ii])])
            s2 = np.array([1.0/ds[ii] for kk in range(ds[ii])])
            df = ot.sinkhorn(s1, s2, c, lambd)
            f = np.zeros((l, l))
            f[:bs[ii], :ds[ii]] = df
            flows.append(f)
        flows = np.array(flows, dtype=np.float32)
        flows = flows.swapaxes(0,1)
        flows = flows.swapaxes(1,2)
        return flows
    #training the LSTM
    def train_lstm(
                    self,
                    train,
                    test,
                    max_epochs=10,
                    showtest=True,
                    batchsize=32
                ):
        """
        Parameters
        -----------------------
        train: training dataset, list of (model, student, score), score lie in [0,5]
        max_epochs: maximum number of epochs
        test: test dataset, same format as training
        max_epochs: maximum number of epochs to train
        showtest: if True, show test performance after every epoch
        batchsize: size of each batch to process at once.
        """
        print "Training"
        crer=[]
        cr=1.6
        freq=0
        dfreq=40#display frequency
        valfreq=800# Validation frequency
        lrate=0.0001
        precision=2
        for eidx in xrange(0,max_epochs):
            sta=time.time()
            num=len(train)
            nd=eidx
            sta=time.time()
            print 'Epoch',eidx
            rnd=sample(xrange(len(train)),len(train))
            for i in range(0,num,batchsize):
                ste = time.time()
                q=[]
                x=i+batchsize
                if x>num:
                    x=num
                for z in range(i,x):
                    q.append(train[rnd[z]])
                x1, mas1, x2, mas2, y, y2 = prepare_data(q)
                ls=[]
                ls2=[]
                freq+=1
                use_noise.set_value(1.)
                for j in range(0,len(x1)):
                    ls.append(embed(x1[j]))
                    ls2.append(embed(x2[j]))
                trconv=np.dstack(ls)
                trconv2=np.dstack(ls2)
                emb2=np.swapaxes(trconv2,1,2)
                emb1=np.swapaxes(trconv,1,2)
                dm = self.f_dm(emb1, mas1, emb2, mas2)
                flows = self.solve_emd_fast(dm, mas1, mas2)
                cst=self.f_grad_shared(emb2, mas2, emb1, mas1, y2, flows)
                s=self.f_update(lrate)
                if np.mod(freq,dfreq)==0:
                    print 'Epoch ', eidx, 'Update ', freq, 'Cost ', cst
                ete = time.time()
            sto=time.time()
            print "epoch took:",sto-sta
        if showtest:
            print "Test error -",self.chkterr2(test)

    def round_the_pred(self, pred):
        """
        Rounds the prediction value to nearest 0.5. Used with L2 loss
        """
        rp = []
        for p in pred:
            diff = p-int(p)
            intp = int(p)
            if diff < 0.25:
                rp.append(intp)
            elif diff < 0.75:
                rp.append(intp+0.5)
            else:
                rp.append(int(p)+1)
        return rp

    def get_error(self, data):
        """
        Returns MSE, MAE, Pearson and Spearman scores
        Parameters:
        ---------------------
        data: list of (model, student, score)
        """
        count=[]
        num=len(data)
        px=[]
        yx=[]
        use_noise.set_value(0.)
        for i in range(0,num,256):
            q=[]
            x=i+256
            if x>num:
                x=num
            for j in range(i,x):
                q.append(data[j])
            x1, mas1, x2, mas2, y, y2 = prepare_data(q)#y is actual score, y2 is class number
            ls=[]
            ls2=[]
            for j in range(0,len(q)):
                ls.append(embed(x1[j]))
                ls2.append(embed(x2[j]))
            trconv=np.dstack(ls)
            trconv2=np.dstack(ls2)
            emb2=np.swapaxes(trconv2,1,2)
            emb1=np.swapaxes(trconv,1,2)
            dm = self.f_dm(emb1, mas1, emb2, mas2)
            flows = self.solve_emd_fast(dm, mas1, mas2)
            pred=self.f_pred(emb1, mas1, emb2, mas2, flows)
            for z in range(0,len(q)):
                yx.append(y[z])
                px.append(pred[z])
        px=np.array(px)
        yx=np.array(yx)
        return np.mean((px-yx)**2), np.mean(abs(px-yx)),meas.pearsonr(px,yx)[0],meas.spearmanr(yx,px)[0]

    def predict_score(self, sa, sb):
        q=[[sa,sb,0]]
        x1,mas1,x2,mas2,y2=prepare_data(q)
        ls=[]
        ls2=[]
        use_noise.set_value(0.)
        for j in range(0,len(x1)):
            ls.append(embed(x1[j]))
            ls2.append(embed(x2[j]))
        trconv=np.dstack(ls)
        trconv2=np.dstack(ls2)
        emb2=np.swapaxes(trconv2,1,2)
        emb1=np.swapaxes(trconv,1,2)
        dm = self.f_dm_l1(emb1, mas1, emb2, mas2)
        flows = self.solve_emd_fast(dm, mas1, mas2)
        return self.f_pred(emd1, mas1, emd2, mas2, flows)

def numpy_floatX(data):
    return numpy.asarray(data, dtype=config.floatX)

def zipp(params, tparams):
    for kk, vv in params.iteritems():
        tparams[kk].set_value(vv)
        delta = 0.1

def huber(target, output):
    d = target - output
    a = .5 * d**2
    b = delta * (abs(d) - delta / 2.)
    l = T.switch(abs(d) <= delta, a, b)
    return l.sum()

def unzip(zipped):
    new_params = OrderedDict()
    for kk, vv in zipped.iteritems():
        new_params[kk] = vv.get_value()
    return new_params

def init_tparams(params):
    tparams = OrderedDict()
    for kk, pp in params.iteritems():
        tparams[kk] = theano.shared(params[kk], name=kk)
    return tparams

def get_layer(name):
    fns = layers[name]
    return fns
# In[2]:
def genm(mu,sigma,n1,n2):
    #return np.ones((n1,n2))
    return np.random.normal(mu,sigma,(n1,n2))

def getlayerx(d,pref,n,nin):
    mu=0.0
    sigma=0.2
    U = np.concatenate([genm(mu,sigma,n,n),genm(mu,sigma,n,n),genm(mu,sigma,n,n),genm(mu,sigma,n,n)])/np.sqrt(n)
    U=np.array(U,dtype=np.float32)
    W =np.concatenate([genm(mu,sigma,n,nin),genm(mu,sigma,n,nin),genm(mu,sigma,n,nin),genm(mu,sigma,n,nin)])/np.sqrt(np.sqrt(n*nin))
    W=np.array(W,dtype=np.float32)

    d[_p(pref, 'U')] = U
    #b = numpy.zeros((n * 300,))+1.5
    b = np.random.uniform(-0.5,0.5,size=(4*n,))
    b[n:n*2]=1.5
    d[_p(pref, 'W')] = W
    d[_p(pref, 'b')] = b.astype(config.floatX)
    return d

def creatrnnx():
    newp=OrderedDict()
    newp=getlayerx(newp,'1lstm1',50,300)
    newp=getlayerx(newp,'1lstm2',50,300)
    newp=getlayerx(newp,'2lstm1',50,300)
    newp=getlayerx(newp,'2lstm2',50,300)
    return newp

def dropout_layer(state_before, use_noise, rrng,rate):
    proj = tensor.switch(use_noise,
                         (state_before *rrng),
                         state_before * (1-rate))
    return proj

def getpl2(prevlayer,pre,mymask,used,rrng,size,tnewp):
    proj = lstm_layer2(tnewp, prevlayer, options,
                                        prefix=pre,
                                        mask=mymask,nhd=size)
    if used:
        print "Added dropout"
        proj = dropout_layer(proj, use_noise, rrng,0.5)

    return proj

def lstm_layer2(tparams, state_below, options, prefix='lstm', mask=None,nhd=None):
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1
    assert mask is not None

    def _slice(_x, n, dim):
            if _x.ndim == 3:
                return _x[:, :, n * dim:(n + 1) * dim]
            return _x[:, n * dim:(n + 1) * dim]

    def _step(m_, x_, h_, c_):
            preact = tensor.dot(h_, tparams[_p(prefix, 'U')].T)
            preact += x_
            preact += tparams[_p(prefix, 'b')]
            i = tensor.nnet.sigmoid(_slice(preact, 0, nhd))
            f = tensor.nnet.sigmoid(_slice(preact, 1, nhd))
            o = tensor.nnet.sigmoid(_slice(preact, 2, nhd))
            c = tensor.tanh(_slice(preact, 3, nhd))
            c = f * c_ + i * c
            c = m_[:, None] * c + (1. - m_)[:, None] * c_
            h = o * tensor.tanh(c)
            h = m_[:, None] * h + (1. - m_)[:, None] * h_
            return [h, c]

    state_below = (tensor.dot(state_below, tparams[_p(prefix, 'W')].T) + tparams[_p(prefix, 'b')].T)
    #print "hvals"
    dim_proj = nhd
    [hvals,yvals], updates = theano.scan(_step,
                                sequences=[mask, state_below],
                                outputs_info=[tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj),
                                              tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj)],
                                name=_p(prefix, '_layers'),
                                n_steps=nsteps)
    return hvals

def adadelta(lr, tparams, grads, emb11,mask11,emb21, mask21, y, cost, flows):
    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='%s_grad' % k)
                    for k, p in tparams.iteritems()]
    running_up2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                 name='%s_rup2' % k)
                   for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='%s_rgrad2' % k)
                      for k, p in tparams.iteritems()]
    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rg2up = [(rg2, (0.95 * rg2 + 0.05* (g ** 2)))
             for rg2, g in zip(running_grads2, grads)]
    f_grad_shared = theano.function([emb11,mask11,emb21,mask21,y,flows], cost, updates=zgup + rg2up,
                                    name='adadelta_f_grad_shared')
    updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
             for zg, ru2, rg2 in zip(zipped_grads,
                                     running_up2,
                                     running_grads2)]
    ru2up = [(ru2, (0.95 * ru2 + 0.05 * (ud ** 2)))
             for ru2, ud in zip(running_up2,updir)]
    param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]
    f_update = theano.function([lr], [], updates=ru2up + param_up,
                               on_unused_input='ignore',
                               name='adadelta_f_update')
    return f_grad_shared, f_update

def sgd(lr, tparams, grads, emb11,mask11,emb21,mask21,y, cost):

    gshared = [theano.shared(p.get_value() * 0., name='%s_grad' % k)
               for k, p in tparams.iteritems()]
    gsup = [(gs, g) for gs, g in zip(gshared, grads)]
    f_grad_shared = theano.function([emb11,mask11,emb21,mask21,y], cost, updates=gsup,
                                    name='sgd_f_grad_shared')
    pup = [(p, p - lr * g) for p, g in zip(tparams.values(), gshared)]
    f_update = theano.function([lr], [], updates=pup,
                               name='sgd_f_update')
    return f_grad_shared, f_update

def rmsprop(lr, tparams, grads, emb11,mask11,emb21,mask21,y, cost):
    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='%s_grad' % k)
                    for k, p in tparams.iteritems()]
    running_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                   name='%s_rgrad' % k)
                     for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='%s_rgrad2' % k)
                      for k, p in tparams.iteritems()]
    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rgup = [(rg, 0.95 * rg + 0.05 * g) for rg, g in zip(running_grads, grads)]
    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
             for rg2, g in zip(running_grads2, grads)]
    f_grad_shared = theano.function([emb11,mask11,emb21,mask21,y], cost,
                                    updates=zgup + rgup + rg2up,
                                    name='rmsprop_f_grad_shared')
    updir = [theano.shared(p.get_value() * numpy_floatX(0.),
                           name='%s_updir' % k)
             for k, p in tparams.iteritems()]
    updir_new = [(ud, 0.9 * ud - 1e-4 * zg / tensor.sqrt(rg2 - rg ** 2 + 1e-4))
                 for ud, zg, rg, rg2 in zip(updir, zipped_grads, running_grads,
                                            running_grads2)]
    param_up = [(p, p + udn[1])
                for p, udn in zip(tparams.values(), updir_new)]
    f_update = theano.function([lr], [], updates=updir_new + param_up,
                               on_unused_input='ignore',
                               name='rmsprop_f_update')
    return f_grad_shared, f_update
    d2=pickle.load(open("synsem.p",'rb'))

dtr=pickle.load(open("dwords.p",'rb'))
prefix='lstm'
noise_std=0.
use_noise = theano.shared(numpy_floatX(0.))
flg=1
cachedStopWords=stopwords.words("english")
training=False #Loads best saved model if False
Syn_aug=True # If true, performs better on Test dataset but longer training time
options=locals().copy()
